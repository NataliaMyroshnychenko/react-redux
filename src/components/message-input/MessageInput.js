import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {addMessage} from '../../chat/actions';
import defaultUserConfig from '../../shared/config/defaultUserConfig';
import './MessageInput.css';
import {connect} from "react-redux";

class MessageInput extends Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultUserData();
        this.onChangeData = this.onChangeData.bind(this);
        this.onSaveMessage = this.onSaveMessage.bind(this);
    }

    onSaveMessage = (event) => {
        event.preventDefault()
        this.props.addMessage(this.state);
        this.setState(this.getDefaultUserData());
        this.input.value = ""
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.messageId !== this.props.messageId) {
            const message = this.props.messages.find(message => message.id === nextProps.messageId);
            this.setState(message);
        }
    }

    getDefaultUserData() {
        return {
            ...defaultUserConfig
        };
    }

    componentDidMount = () => {
        this.input.focus()
    }

    handleFormSubmit = (event) => {
        event.preventDefault()
        this.onMessageSend(this.input.value)
        this.input.value = ""
    }

    onChangeData(e) {
        const value = e.target.value;
        this.setState(
            {
                ...this.state,
                "userId": "Me",
                "avatar": "",
                "user": "Me",
                "text": value,
                "createdAt": new Date().toISOString(),
                "editedAt": ""
            }
        );
    }

    render() {
        return (
            <form className="message-input" onSubmit={this.onSaveMessage}>
                <div className="message-input-text">
                    <input
                        type="text"
                        ref={(node) => (this.input = node)}
                        placeholder="Enter your message..."
                        onChange={this.onChangeData}
                    />
                </div>
                <div className="message-input-button">
                    <button onClick={this.onSaveMessage}>
                        Send
                    </button>
                </div>
            </form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.data
    }
};

const mapDispatchToProps = {
    addMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);




