import React, {Component} from "react";
import {connect} from 'react-redux'
import * as actions from './actions';
import {addMessage, updateMessage} from '../chat/actions';
import TextInput from './TextInput';
import userFormConfig from '../shared/config/messageFormConfig.json';
import defaultUserConfig from '../shared/config/defaultUserConfig';
import {dropCurrentMessageId} from "./actions";

class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultUserData();
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onChangeData = this.onChangeData.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.messageId !== this.props.messageId) {
            const message = this.props.messages.data.find(message => message.id === nextProps.messageId);
            this.setState(message);
        }
    }

    onCancel() {
        this.props.dropCurrentMessageId();
        this.props.hidePage();
        this.setState(this.getDefaultUserData());
    }

    onSave() {
        if (this.props.messageId) {
            this.props.updateMessage(this.props.messageId, this.state);
        } else {
            this.props.addMessage(this.state);
        }
        this.props.dropCurrentMessageId();
        this.props.hidePage();
        this.setState(this.getDefaultUserData());
    }

    onChangeData(e, keyword) {
        const value = e.target.value;
        this.setState(
            {
                ...this.state,
                [keyword]: value
            }
        );
    }

    getDefaultUserData() {
        return {
            ...defaultUserConfig
        };
    }

    getInput(data, {label, type, keyword}) {
        return (
            <TextInput
                label={label}
                type={type}
                text={data[keyword]}
                keyword={keyword}
                onChange={this.onChangeData}
            />
        );
    }

    getUserPageContent() {
        const data = this.state;

        return (
            <div className="edit-message-modal">
            <div className="modal-shown modal" style={{display: "block"}} tabIndex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{padding: "5px"}}>
                        <div className="modal-header">
                            <h5 className="modal-title">Edit message</h5>
                            <button type="button" className="edit-message-close close" data-dismiss="modal" aria-label="Close"
                                    onClick={this.onCancel}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="edit-message-input modal-body">
                            {
                                userFormConfig.map(item =>
                                    this.getInput(data, item)
                                )
                            }
                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-secondary" onClick={this.onCancel}>Cancel</button>
                            <button className="edit-message-button btn btn-primary" onClick={this.onSave}>Save</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        );
    }

    render() {
        const isShown = this.props.isShown;
        return isShown ? this.getUserPageContent() : null;
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages,
        isShown: state.editPage.isShown,
        messageId: state.editPage.messageId
    }
};

const mapDispatchToProps = {
    ...actions,
    addMessage,
    updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
