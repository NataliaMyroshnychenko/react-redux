import {
    LOAD_DATA, MESSAGES_HAS_ERRORED, MESSAGES_IS_LOADING, MESSAGES_FETCH_DATA_SUCCESS,
    ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, LIKED_MESSAGE
} from "./actionTypes";
import service from './service';

export function messagesHasErrored(bool) {
    return {
        type: MESSAGES_HAS_ERRORED,
        hasErrored: bool
    };
}

export function messagesIsLoading(bool) {
    return {
        type: MESSAGES_IS_LOADING,
        isLoading: bool
    };
}

export function messagesFetchDataSuccess(messages) {
    return {
        type: MESSAGES_FETCH_DATA_SUCCESS,
        messages: messages
    };
}

export function messagesFetchData(url) {
    return (dispatch) => {
        dispatch(messagesIsLoading(true));

        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                dispatch(messagesIsLoading(false));
                return response;
            })
            .then((response) => response.json())
            .then((messages) => dispatch(messagesFetchDataSuccess(messages)))
            .catch(() => dispatch(messagesHasErrored(true)));
    };
}

export const loadData = (data) => ({
    type: LOAD_DATA,
    payload: {
        data
    }
});

export const addMessage = data => ({
    type: ADD_MESSAGE,
    payload: {
        id: service.getNewId(),
        data
    }
});

export const updateMessage = (id, data) => ({
    type: UPDATE_MESSAGE,
    payload: {
        id,
        data
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const likesMessage = (id, like) => ({
    type: LIKED_MESSAGE,
    payload: {
        id,
        like: 1
    }
});
