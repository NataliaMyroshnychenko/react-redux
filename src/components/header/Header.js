import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './Header.css';

class Header extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        countParticipants: PropTypes.number,
    }

    getCountParticipants = () => {
        return this.props.countParticipants;
    }

    getCountMessages() {
        return this.props.countMessages;
    }

    getDateTimeLastMessage = () => {
        return this.props.dateTimeLastMessage;
    }

    render() {

        return (
            <div className="header">
                <div className="header-title">
                    <h2>Chat</h2>
                </div>
                <div className="header-users-count">
                    {this.getCountParticipants()} participants
                </div>
                <div className="header-messages-count">
                    {this.getCountMessages()} messages
                </div>
                <div className="header-last-message-date">
                    last message at {moment((this.getDateTimeLastMessage())).format('DD.MM.yyyy HH:mm')}
                </div>
            </div>
        )
    }
}

export default Header;
