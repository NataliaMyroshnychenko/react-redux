import React from 'react';
import {Provider} from "react-redux";
import {render} from 'react-dom';
import ReactDOM from 'react-dom';
import Chat from './chat';
import reportWebVitals from './reportWebVitals';
import configureStore from "./store";
import './index.css';
import * as serviceWorker from './serviceWorker';
import UserPage from "./editPage";
import 'bootstrap/dist/css/bootstrap.css';

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Chat url={"https://edikdolynskyi.github.io/react_sources/messages.json"}/>
        <UserPage/>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals


serviceWorker.unregister();

