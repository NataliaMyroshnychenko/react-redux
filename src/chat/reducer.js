import {
    LOAD_DATA,
    ADD_MESSAGE,
    UPDATE_MESSAGE,
    DELETE_MESSAGE,
    LIKED_MESSAGE,
    MESSAGES_HAS_ERRORED,
    MESSAGES_IS_LOADING,
    MESSAGES_FETCH_DATA_SUCCESS
} from "./actionTypes";

const initialState = {
    "isLoading": true,
    "hasErrored": false,
    "data": [{
        id: "",
        userId: "",
        avatar: "",
        user: "",
        text: "",
        createdAt: "",
        editedAt: ""
    }],
};

export function messages(state = initialState.data, action) {
    switch (action.type) {
        case LOAD_DATA: {
            const {data} = action.payload;
            return [
                ...state,
                [data]
            ];
        }

        case ADD_MESSAGE: {
            const {id, data} = action.payload;
            const newMessage = {id, ...data};
            return {
                ...state,
                data: [...state.data, newMessage]
            }
        }

        case UPDATE_MESSAGE: {
            const {id, data} = action.payload;
            const updatedMessage = state.data.map(message => {
                if (message.id === id) {
                    return {
                        ...message,
                        ...data
                    };
                } else {
                    return message;
                }
            });
            return {
                ...state,
                data: updatedMessage
            };
        }

        case DELETE_MESSAGE: {
            const {id} = action.payload;
            const filteredMessage = state.data.filter(message => message.id !== id);
            return {
                ...state,
                data: filteredMessage
            };
        }

        case MESSAGES_FETCH_DATA_SUCCESS:
            return {
                ...state,
                data: action.messages,
            }
        default:
            return state;
    }
}

export function messagesHasErrored(state = initialState.hasErrored, action) {
    switch (action.type) {
        case MESSAGES_HAS_ERRORED:
            return action.hasErrored;

        default:
            return state;
    }
}

export function messagesIsLoading(state = initialState.isLoading, action) {
    switch (action.type) {
        case MESSAGES_IS_LOADING:
            return {
                isLoading: action.isLoading
            };

        default:
            return state;
    }
}
