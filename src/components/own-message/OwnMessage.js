import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './OwnMessage.css';

class OwnMessage extends Component {
    constructor(props) {
        super(props);
        this.handleDeleteMessage = this.handleDeleteMessage.bind(this);
    }

    static propTypes = {
        id: PropTypes.string,
        user: PropTypes.string,
        text: PropTypes.string.isRequired,
        createdAt: PropTypes.string,
        messages: PropTypes.arrayOf(PropTypes.object),
        onMessageDelete: PropTypes.func.isRequired
    }

    handleFormEdit = (event) => {
        event.preventDefault();
        this.props.onMessageEdit(this.props.id);
    }

    handleDeleteMessage = (event) => {
        event.preventDefault();
        this.props.onMessageDelete(this.props.id);
    }

    colorLike() {
        document.getElementById('like').onclick = function () {
            this.style.backgroundColor = "red";
        }
    }

    render() {
        return (
            <div className="own-message">
                <div className="message-text">
                    {this.props.text}
                </div>
                <div className="message-time">
                    {moment(this.props.createdAt).format('HH:mm')}
                </div>
                <div className="message-edit">
                    <button onClick={this.handleFormEdit}>
                        Edit
                    </button>
                </div>
                <div className="message-delete">
                    <button
                        onClick={this.handleDeleteMessage}
                    >
                        Delete
                    </button>
                </div>
            </div>
        )
    }
}

export default OwnMessage;
