import React, {Component} from 'react';
import logo from '../preloader/processing.gif';

import './Preloader.css';

class Preloader extends Component {
    constructor(props) {
        super(props);
    }

    getPreloader(isLoading) {
        if (isLoading) {
            return (
                <div className="preloader">
                    <img id="preloader-id" src={logo} className="preloader-logo" alt="logo"/>
                </div>);
        } else {
            return null;
        }
    }

    render() {
        return (
            this.getPreloader(this.props.isLoading)
        )
    }
}

export default Preloader;
