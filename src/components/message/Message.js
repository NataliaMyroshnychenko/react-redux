import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './Message.css';
import moment from 'moment';

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            black: false
        }
    }

    changeColor() {
        this.setState({black: !this.state.black})
    }

    static propTypes = {
        user: PropTypes.string,
        text: PropTypes.string.isRequired,
        createdAt: PropTypes.string,
        me: PropTypes.bool,
    }


    colorLike() {
        document.getElementById('like').onclick = function () {
            this.style.backgroundColor = "red";
        }
    }

    render() {
        let btn_class = this.state.black ? "blackButton" : "whiteButton";

        return (

            <div className="message">
                <div className="message-user-avatar">

                    <img src={this.props.avatar} alt="Аватар"/>
                </div>
                <div className="message-user-name">
                    {this.props.user}
                </div>
                <div className="message-text">
                    {this.props.text}
                </div>
                <div className="message-time">
                    {moment(this.props.createdAt).format('HH:mm')}
                </div>
                <div className="message-like">
                    <button className={btn_class}
                            onClick={this.changeColor.bind(this)}>
                        {/*<i className="bi bi-heart"></i>*/}
                        Like
                    </button>
                </div>

            </div>

        )
    }
}

export default Message;
