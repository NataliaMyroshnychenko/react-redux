import React, {Component} from 'react'
import logo from '../logo.jpg';
import {connect} from 'react-redux'
import * as actions from './actions';
import {setCurrentMessageId, showPage} from '../editPage/actions';
import './index.css';
import Header from '../components/header/Header.js';
import MessageInput from '../components/message-input/MessageInput.js';
import MessageList from '../components/message-list/MessageList.js';
import Preloader from '../components/preloader/Preloader';
import defaultUserConfig from '../config/defaultUserConfig';
import '../components/preloader/Preloader.css';
import {deleteMessage, messagesFetchData, messagesIsLoading} from "./actions";

class Chat extends Component {
    constructor(props) {
        super(props)
        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.state = this.getDefaultUserData();
    }

    componentDidMount() {
        this.props.fetchData(this.props.url);
        this.props.messagesIsLoading(false);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.id !== this.props.id) {
            const message = this.props.messages.find(message => message.id === nextProps.id);
            this.setState(message);
        }
    }

    getDefaultUserData() {
        return {
            ...defaultUserConfig
        };
    }

    handlerCountParticipants = () => {
        const list = new Set();
        this.props.messages.data?.map((message, i) => {
            list.add(message.user);
        });
        return list.size;
    }

    handlerCountMessages() {
        return this.props.messages.data?.length;
    }

    handlerDateTimeLastMessage = () => {
        var messages = this.props.messages.data;
        if (messages != undefined) {
            var lastMessage = this.props.messages?.data[this.props.messages.data.length - 1];
            var lastdate = lastMessage?.createdAt;
            return lastdate;
        } else {
            return null;
        }
        return "";
    }

    sortByDate(messages) {
        var sortMessages = messages.sort(function (a, b) {
            var c = new Date(a.createdAt).getTime();
            var d = new Date(b.createdAt).getTime();
            if (c > d) {
                return 1;
            }
            if (c < d) {
                return -1;
            }
            return 0;
        });
        return sortMessages;
    }

    onEdit(id) {
        this.props.setCurrentMessageId(id);
        this.props.showPage();
    }

    onDelete(id) {
        this.props.deleteMessage(id);
    }

    onAdd() {
        this.props.showPage();
    }

    render() {
        if (this.props.hasErrored) {
            return <p>Sorry! There was an error loading the items</p>;
        }
        return (
            <div className="chat">
                <Preloader isLoading={this.props.isLoading}/>
                <Header
                    countParticipants={this.handlerCountParticipants()}
                    countMessages={this.handlerCountMessages()}
                    dateTimeLastMessage={this.handlerDateTimeLastMessage()}
                    messages={this.state.messages.data}
                />
                <MessageList
                    messages={this.props.messages.data}
                    onMessageDelete={this.onDelete}
                    onMessageEdit={this.onEdit}
                />
                <MessageInput/>
                {/*<UserPage/>*/}

                <header className="logo-wrapper">
                    <img src={logo} className="main-logo" alt="logo"/>
                </header>
            </div>

        );
    }
}

const mapStateToProps = (state) => {

    return {
        messages: state.messages,
        data: state.messages.data,
        hasErrored: state.messagesHasErrored,
        isLoading: state.messagesIsLoading.isLoading,
        isShown: state.editPage.isShown,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        ...actions,
        setCurrentMessageId: (id) => dispatch(setCurrentMessageId(id)),
        showPage: (id) => dispatch(showPage()),
        fetchData: (url) => dispatch(messagesFetchData(url)),
        messagesIsLoading,
        deleteMessage: (id) => dispatch(deleteMessage(id))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
