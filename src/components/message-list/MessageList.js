import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Message from '../message/Message.js';

import './MessageList.css';
import OwnMessage from '../own-message/OwnMessage.js';

class MessageList extends Component {
    constructor(props) {
        super(props);
        this.onMessageDelete = this.onMessageDelete.bind(this);
        this.onMessageEdit = this.onMessageEdit.bind(this);
    }

    static propTypes = {
        messages: PropTypes.arrayOf(PropTypes.object)
    }

    userMessage(message, i) {
        const isUser = message.user;
        if (isUser === "Me") {
            return <OwnMessage
                key={i} {...message}
                onMessageDelete={this.props.onMessageDelete}
                onMessageEdit={this.props.onMessageEdit}
            />;
        }
        return <Message key={i} {...message} />;
    }

    onMessageDelete(id) {
        this.props.onMessageDelete(id);
    }

    onMessageEdit(id) {
        this.props.onMessageEdit(id);
    }

    setDivider() {
        let div = document.createElement('div');
        div.id = 'message-divider';
        document.getElementsByClassName("messageList")[0].appendChild(div);
    }

    render() {
        return (
            <div className="messageList">
                {this.props.messages?.map((message, i) => (
                    this.userMessage(message, i)
                ))}
            </div>
        );
    }
}

export default MessageList;
