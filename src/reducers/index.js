import {combineReducers} from "redux";
import {messages, messagesHasErrored, messagesIsLoading} from "../chat/reducer";
import editPage from "../editPage/reducer";

const rootReducer = combineReducers({
    messages,
    editPage,
    messagesHasErrored,
    messagesIsLoading,
});

export default rootReducer;

